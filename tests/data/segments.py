
segment_a = [[0, 0], [0, 1]]

segment_b = [[0, 1], [0, 0]]

segment_c = [[0, 0], [0, -1]]

segment_d = [[0, -1], [0, 0]]

segment_e = [[-1, -1], [1, 1]]

segment_f = [[1, 1], [-1, -1]]

segment_g = [[1, -1], [-1, 1]]

segment_h = [[-1, -1], [-1, 1]]

segment_i = [[1, -1], [1, 1]]

segment_list = [
    segment_a,
    segment_b,
    segment_c,
    segment_d,
    segment_e,
    segment_f,
    segment_g,
    segment_h,
    segment_i
]