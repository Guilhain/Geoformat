geolayer_fr_dept_population_CODE_DEPT_hash_index = {
    "metadata": {"type": "hashtable"},
    "index": {
        "32": [0],
        "47": [1],
        "38": [2],
        "62": [3],
        "08": [4],
        "10": [5],
        "42": [6],
        "06": [7],
        "31": [8],
        "71": [9],
        "53": [10],
        "78": [11],
        "50": [12],
        "16": [13],
        "25": [14],
        "55": [15],
        "33": [16],
        "14": [17],
        "88": [18],
        "18": [19],
        "07": [20],
        "02": [21],
        "64": [22],
        "41": [23],
        "57": [24],
        "86": [25],
        "24": [26],
        "39": [27],
        "82": [28],
        "49": [29],
        "69": [30],
        "12": [31],
        "23": [32],
        "45": [33],
        "70": [34],
        "63": [35],
        "81": [36],
        "27": [37],
        "76": [38],
        "52": [39],
        "30": [40],
        "67": [41],
        "11": [42],
        "77": [43],
        "43": [44],
        "51": [45],
        "80": [46],
        "46": [47],
        "65": [48],
        "04": [49],
        "72": [50],
        "56": [51],
        "2A": [52],
        "28": [53],
        "54": [54],
        "01": [55],
        "19": [56],
        "09": [57],
        "68": [58],
        "59": [59],
        "90": [60],
        "44": [61],
        "89": [62],
        "35": [63],
        "40": [64],
        "29": [65],
        "74": [66],
        "60": [67],
        "95": [68],
        "58": [69],
        "61": [70],
        "91": [71],
        "21": [72],
        "22": [73],
        "03": [74],
        "17": [75],
        "15": [76],
        "34": [77],
        "26": [78],
        "66": [79],
        "73": [80],
        "37": [81],
        "05": [82],
        "79": [83],
        "84": [84],
        "36": [85],
        "2B": [86],
        "87": [87],
        "85": [88],
        "83": [89],
        "94": [90],
        "92": [91],
        "48": [92],
        "13": [93],
        "93": [94],
        "75": [95],
    },
}

geolayer_fr_dept_population_INSEE_REG_hash_index = {
    "metadata": {"type": "hashtable"},
    "index": {
        "76": [0, 8, 28, 31, 36, 40, 42, 47, 48, 57, 77, 79, 92],
        "75": [1, 13, 16, 22, 25, 26, 32, 56, 64, 75, 83, 87],
        "84": [2, 6, 20, 30, 35, 44, 55, 66, 74, 76, 78, 80],
        "32": [3, 21, 46, 59, 67],
        "44": [4, 5, 15, 18, 24, 39, 41, 45, 54, 58],
        "93": [7, 49, 82, 84, 89, 93],
        "27": [9, 14, 27, 34, 60, 62, 69, 72],
        "52": [10, 29, 50, 61, 88],
        "11": [11, 43, 68, 71, 90, 91, 94, 95],
        "28": [12, 17, 37, 38, 70],
        "24": [19, 23, 33, 53, 81, 85],
        "53": [51, 63, 65, 73],
        "94": [52, 86],
    },
}
