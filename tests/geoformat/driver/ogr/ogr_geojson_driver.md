# geojson driver with OGR

## metadata
Unsurprisingly, metadata information (width, precision) outside of type is imprecise on the geojson driver.

## attributes

OGR geojson driver does not allow binary fields -> ideally they should be converted to hexadecimal text

## problem with ogr driver

Date / Time and Datetime type are not well converted to iso format. For example the value iso 2020-03-31 11:22:10.000999
 gives 2020/03/31 11:22:10.001 with ogr. Iso separator '-' is converted to '/'.

