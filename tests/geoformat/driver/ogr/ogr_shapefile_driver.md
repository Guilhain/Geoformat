# shapefile driver with OGR

## metadata
- Field names must not exceed 10 characters in length.
- Field width cannot be superior than 254.
- Field type (IntegerList, RealList, StringList, Time, Binary) are not allowed.
- Field type DateTime is automatically convert to type Date.
- Field type Boolean is automatically convert to type Integer.
- Field type DateTime is automatically convert to type Date.

### attributes

OGR geojson driver does not allow binary fields -> ideally they should be converted to hexadecimal text

## problem with ogr driver

### attributes

Date / Time and Datetime type are not well converted to iso format. For example the value iso 2020-03-31 11:22:10.000999
 gives 2020/03/31 11:22:10.001 with ogr. Iso separator '-' is converted to '/'.

