# How to build

## install twine

`pip install build twine`

## build from pyproject.toml

`python -m build`

## publish

`twine upload dist/*`