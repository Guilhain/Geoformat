| Property                   | Circle | Point | Line | Polygon | Matplotlib Equivalent                 | Description                                                                   |
|----------------------------|--------|-------|------|---------|---------------------------------------|-------------------------------------------------------------------------------|
| **Layout**                 | -      |       |      |         |                                       | **Layout Properties**                                                         |
| `icon-image`               | -      | Yes   | -    | -       | N/A                                   | Matplotlib doesn't use icon images in the same way.                           |
| `icon-size`                | -      | Yes   | -    | -       | `markersize` in `plot`/`scatter`      | Size of the marker.                                                           |
| `icon-rotate`              | -      | Yes   | -    | -       | N/A                                   | Rotation of markers isn't a direct feature, though could be emulated.         |
| `icon-offset`              | -      | Yes   | -    | -       | N/A                                   | Direct offsetting of markers isn't a typical feature.                         |
| `text-field`               | -      | Yes   | -    | -       | `text`                                | Adding text annotations to the plot.                                          |
| `text-size`                | -      | Yes   | -    | -       | `fontsize` in `text`                  | Size of the text.                                                             |
| `text-transform`           | -      | Yes   | -    | -       | N/A                                   | Direct text transformations are not supported in the same way.                |
| `text-offset`              | -      | Yes   | -    | -       | N/A                                   | Direct offsetting of text isn't a typical feature.                            |
| `line-cap`                 | -      | -     | Yes  | -       | N/A                                   | Matplotlib doesn't have a direct equivalent; line caps are typically not adjustable. |
| `line-join`                | -      | -     | Yes  | -       | `joinstyle` in `plot`                 | Defines the shape used at the corners of polygons or the segments of a multiline. |
| `line-miter-limit`         | -      | -     | Yes  | -       | `miterlimit` in `plot`                | Determines the limit at which a sharp corner is cut off.                      |
| `line-round-limit`         | -      | -     | Yes  | -       | N/A                                   | No direct equivalent in matplotlib for adjusting round joins.                  |
| **Paint**                  |        |       |      |         |                                       | **Paint Properties for Circle**                                                  |
| `circle-radius`            | Yes    | -     | -    | -       | `radius` in `Circle`                  | Defines the radius of the circle. Can vary based on a data property.            |
| `circle-color`             | Yes    | -     | -    | -       | `facecolor` in `Circle`               | Fill color of the circle.                                                        |
| `circle-blur`              | Yes    | -     | -    | -       | N/A                                   | Blur effect on the circle. Not directly supported in matplotlib.                |
| `circle-opacity`           | Yes    | -     | -    | -       | `alpha` in `Circle`                   | Transparency of the circle.                                                      |
| `circle-translate`         | Yes    | -     | -    | -       | N/A                                   | Offset of the circle position, not directly supported in matplotlib.            |
| `circle-translate-anchor`  | Yes    | -     | -    | -       | N/A                                   | Whether the circle's offset is relative to the map or the viewport.             |
| `circle-pitch-scale`       | Yes    | -     | -    | -       | N/A                                   | How the circle's radius scales with map pitch.                                  |
| `circle-pitch-alignment`   | Yes    | -     | -    | -       | N/A                                   | Circle's alignment to the map or viewport when the map is pitched.              |
| `circle-stroke-width`      | Yes    | -     | -    | -       | `linewidth` in `Circle`               | Width of the circle's stroke.                                                    |
| `circle-stroke-color`      | Yes    | -     | -    | -       | `edgecolor` in `Circle`               | Color of the circle's stroke.                                                    |
| `circle-stroke-opacity`    | Yes    | -     | -    | -       | Use `alpha` with `edgecolor`          | Opacity of the circle's stroke. Not directly controllable in matplotlib.        |
| `icon-opacity`             | -      | Yes   | -    | -       | `alpha` in `plot`/`scatter`           | Transparency of markers.                                                      |
| `icon-color`               | -      | Yes   | -    | -       | `markerfacecolor` in `plot`/`scatter` | Color of the marker.    |
| `icon-halo-color`          | -      | Yes   | -    | -       | N/A                                   | No direct equivalent, but edge colors can be manipulated.                     |
| `icon-halo-width`          | -      | Yes   | -    | -       | `markeredgewidth` in `scatter`        | Width of the marker edge, not exactly a halo but similar concept.             |
| `text-opacity`             | -      | Yes   | -    | -       | `alpha` in `text`                     | Transparency of text annotations.                                             |
| `text-color`               | -      | Yes   | -    | -       | `color` in `text`                     | Color of text annotations.                                                    |
| `text-halo-color`          | -      | Yes   | -    | -       | N/A                                   | No direct equivalent for text halo in matplotlib.                             |
| `text-halo-width`          | -      | Yes   | -    | -       | N/A                                   | No direct equivalent for halo width in matplotlib.                            |
| `line-opacity`             | -      | -     | Yes  | -       | `alpha` in `plot`                     | Transparency of lines.                                                        |
| `line-color`               | -      | -     | Yes  | -       | `color` in `plot`                     | Color of the line.                                                            |
| `line-width`               | -      | -     | Yes  | -       | `linewidth` in `plot`                 | Width of the line.                                                            |
| `line-gap-width`           | -      | -     | Yes  | -       | N/A                                   | No direct equivalent, but custom line styles can be created.                  |
| `line-offset`              | -      | -     | Yes  | -       | N/A                                   | Direct line offsetting isn't a typical matplotlib feature.                    |
| `line-blur`                | -      | -     | Yes  | -       | N/A                                   | Blurring isn't directly supported in matplotlib.                              |
| `line-dasharray`           | -      | -     | Yes  | -       | `dashes` in `plot`                    | Custom dash patterns for lines.                                               |
| `line-pattern`             | -      | -     | Yes  | -       | N/A                                   | Patterned lines aren't directly supported in matplotlib.                      |
| `fill-antialias`           | -      | -     | -    | Yes     | `antialiased` in `fill`               | Whether to antialias the edges of polygons.                                   |
| `fill-opacity`             | -      | -     | -    | Yes     | `alpha` in `fill`                     | Transparency of polygon fill.                                                 |
| `fill-color`               | -      | -     | -    | Yes     | `color` in `fill`                     | Color of the polygon fill.                                                    |
| `fill-outline-color`       | -      | -     | -    | Yes     | `edgecolor` in `fill`                 | Color of the polygon's edge.                                                  |
| `fill-pattern`             | -      | -     | -    | Yes     | N/A                                   | Filling polygons with patterns isn't directly supported in matplotlib.        |
| `fill-translate`           | -      | -     | -    | Yes     | N/A                                   | Direct translation of fills isn't a typical feature.                          |
| `fill-translate-anchor`    | -      | -     | -    | Yes     | N/A                                   | No equivalent; positioning is absolute in matplotlib.                         |
