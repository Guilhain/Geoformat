| Property                | Point                     | Line                       | Polygon                   | Description                                                                   |
|-------------------------|---------------------------|----------------------------|---------------------------|-------------------------------------------------------------------------------|
| **Layout**              |                           |                            |                           | **Layout Properties**                                                         |
| `icon-image`            | Yes                       | -                          | -                         | Specifies the icon image for the point.                                      |
| `icon-size`             | Yes                       | -                          | -                         | Sets the size of the point's icon.                                            |
| `icon-rotate`           | Yes                       | -                          | -                         | Applies a rotation to the point's icon.                                       |
| `icon-offset`           | Yes                       | -                          | -                         | Offsets the icon from the point's position.                                   |
| `text-field`            | Yes                       | -                          | -                         | Specifies the text to display with the point.                                 |
| `text-size`             | Yes                       | -                          | -                         | The size of the text displayed with the point.                                |
| `text-transform`        | Yes                       | -                          | -                         | Transforms the text displayed with the point.                                 |
| `text-offset`           | Yes                       | -                          | -                         | Offsets the text from the point.                                              |
| `line-cap`              | -                         | Yes                        | -                         | Defines the shape used to terminate the lines.                                |
| `line-join`             | -                         | Yes                        | -                         | Defines the shape used to join line segments.                                 |
| `line-miter-limit`      | -                         | Yes                        | -                         | Determines the limit at which the miter join is cut off.                      |
| `line-round-limit`      | -                         | Yes                        | -                         | Limit for `line-join` of type `round`.                                        |
| **Paint**               |                           |                            |                           | **Paint Properties**                                                          |
| `icon-opacity`          | Yes                       | -                          | -                         | Sets the opacity of the point's icon.                                         |
| `icon-color`            | Yes                       | -                          | -                         | Changes the color of the point's icon.                                        |
| `icon-halo-color`       | Yes                       | -                          | -                         | Color of the halo around the point's icon.                                    |
| `icon-halo-width`       | Yes                       | -                          | -                         | Width of the halo around the point's icon.                                    |
| `text-opacity`          | Yes                       | -                          | -                         | The opacity of the text associated with the point.                            |
| `text-color`            | Yes                       | -                          | -                         | The color of the text associated with the point.                              |
| `text-halo-color`       | Yes                       | -                          | -                         | The color of the halo around the point's text.                                |
| `text-halo-width`       | Yes                       | -                          | -                         | The width of the halo around the point's text.                                |
| `line-opacity`          | -                         | Yes                        | -                         | Sets the opacity of the line.                                                 |
| `line-color`            | -                         | Yes                        | -                         | Specifies the color of the line.                                              |
| `line-width`            | -                         | Yes                        | -                         | Defines the width of the line.                                                |
| `line-gap-width`        | -                         | Yes                        | -                         | Creates a gap around the line.                                                |
| `line-offset`           | -                         | Yes                        | -                         | Offsets the line parallel to its original direction.                          |
| `line-blur`             | -                         | Yes                        | -                         | Applies a blur to the line.                                                   |
| `line-dasharray`        | -                         | Yes                        | -                         | Creates a pattern of dashes for the line.                                     |
| `line-pattern`          | -                         | Yes                        | -                         | Applies a repeated image pattern to the line.                                 |
| `fill-antialias`        | -                         | -                          | Yes                       | Defines if the polygon's edges should be smoothed.                            |
| `fill-opacity`          | -                         | -                          | Yes                       | Sets the opacity of the polygon's filled area.                                |
| `fill-color`            | -                         | -                          | Yes                       | Specifies the fill color of the polygon.                                      |
| `fill-outline-color`    | -                         | -                          | Yes                       | Defines the color of the polygon's outline.                                   |
| `fill-pattern`          | -                         | -                          | Yes                       | Applies a repeated image pattern to the polygon's fill.                       |
| `fill-translate`        | -                         | -                          | Yes                       | Offsets the fill pattern and outline of the polygon.                          |
| `fill-translate-anchor` | -                         | -                          | Yes                       | Specifies whether the fill offset is based on the map or the viewport.        |
