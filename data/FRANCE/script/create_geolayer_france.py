import copy
import geoformat


print(geoformat.__version__)

# get departement attributes data
dpt_bd_topo_path = 'data/FRANCE_IGN/DEPARTEMENT_2016_L93.shp'
geolayer_dpt_bd_topo = geoformat.ogr_layer_to_geolayer(dpt_bd_topo_path)

dico_dpt = {}
for i_feat, feature in geolayer_dpt_bd_topo['features'].items():
    dico_dpt[i_feat] = (feature['attributes']['CODE_DEPT'], feature['attributes']['NOM_DEPT'])

# get departement generalised geometry in Geometry Collection
path = 'data/FRANCE/FRANCE_DPT_GENERALIZE_LAMB93.geojson'
output_path = 'data/FRANCE/'
geolayer = geoformat.ogr_layer_to_geolayer(path)

# extract geometries
for i_feat, feature in geolayer['features'].items():
    if "geometry" in feature:
        if feature['geometry']['type'] == 'GeometryCollection':
            for geometry in feature['geometry']['geometries']:
                coordinates = geoformat.format_coordinates(geometry['coordinates'], precision=0, delete_duplicate_following_coordinates=True)
                # rewrite coordinates in geometry
                geometry['coordinates'] = coordinates

# create 3 geolayers
new_geolayer_data_and_geometry = {'metadata': {}, 'features': {}}
new_geolayer_geometry_only = {'metadata': {}, 'features': {}}
new_geolayer_data_only = {'metadata': {}, 'features': {}}

fields_metadata = {
    'CODE_DEPT': copy.deepcopy(geolayer_dpt_bd_topo['metadata']['fields']['CODE_DEPT']),
    'NOM_DEPT': copy.deepcopy(geolayer_dpt_bd_topo['metadata']['fields']['CODE_DEPT'])
}
fields_metadata['CODE_DEPT']['index'] = 0
fields_metadata['NOM_DEPT']['index'] = 1

new_geolayer_data_and_geometry['metadata']['fields'] = copy.deepcopy(fields_metadata)
new_geolayer_data_only['metadata']['fields'] = copy.deepcopy(fields_metadata)
new_geolayer_data_and_geometry['metadata']['geometry_ref'] = {'type': 'Polygon', 'crs': 2154}
new_geolayer_geometry_only['metadata']['geometry_ref'] = {'type': 'Polygon', 'crs': 2154}

new_i_feat = 0
for i_feat, feature in geolayer['features'].items():
    for i_geom, geometry in enumerate(feature['geometry']['geometries']):
        new_feature = {"geometry": geometry}
        new_geolayer_geometry_only['features'][new_i_feat] = copy.deepcopy(new_feature)
        new_feature["attributes"] = {'CODE_DEPT': dico_dpt[i_geom][0], 'NOM_DEPT': dico_dpt[i_geom][1]}
        new_geolayer_data_and_geometry['features'][new_i_feat] = copy.deepcopy(new_feature)
        del new_feature['geometry']
        new_geolayer_data_only['features'][new_i_feat] = copy.deepcopy(new_feature)
        new_i_feat += 1


# rename
geolayer['metadata']['name'] = 'FRANCE_DPT_GENERALIZE_LAMB93_ROUND'
new_geolayer_geometry_only['metadata']['name'] = 'FRANCE_DPT_GENERALIZE_LAMB93_ROUND_GEOMETRY_ONLY'
new_geolayer_data_only['metadata']['name'] = 'FRANCE_DPT_GENERALIZE_LAMB93_ROUND_DATA_ONLY'
new_geolayer_data_and_geometry['metadata']['name'] = 'FRANCE_DPT_GENERALIZE_LAMB93_ROUND_DATA_AND_GEOMETRY'

print(geoformat.print_metadata_field_table(new_geolayer_data_only))
print(geoformat.print_features_data_table(new_geolayer_data_only))


print(geoformat.print_metadata_field_table(new_geolayer_data_and_geometry))
print(geoformat.print_features_data_table(new_geolayer_data_and_geometry))
# write
geoformat.geolayer_to_ogr_layer(geolayer, output_path, 'geojson')
geoformat.geolayer_to_ogr_layer(new_geolayer_geometry_only, output_path, 'geojson')
geoformat.geolayer_to_ogr_layer(new_geolayer_data_only, output_path, 'geojson')
geoformat.geolayer_to_ogr_layer(new_geolayer_data_and_geometry, output_path, 'geojson')


