import copy
import geoformat
import yaml


print(geoformat.__version__)


# open bus line
bus_lines_path = 'data/FRANCE/BDX_BUS.geojson'
bus_lines_geolayer = geoformat.geojson_to_geolayer(bus_lines_path)
bus_lines_id_field_name = 'id_line_topo'

# open point collision and index (bus line's id)
bus_collision_path = 'data/FRANCE/BDX_BUS_COLLISION.geojson'
bus_collision_geolayer = geoformat.geojson_to_geolayer(bus_collision_path)
bus_collision_id_line_index = geoformat.create_attribute_index(bus_collision_geolayer, bus_lines_id_field_name)

# create ouput format
bus_lines_split_at_collision_path = 'data/FRANCE/BDX_BUS_COLLISION_split.geojson'
bus_lines_split_at_collision_geolayer = {"metadata": bus_lines_geolayer['metadata'], 'features': {}}
# rename geolayer
bus_lines_split_at_collision_geolayer['metadata']['name'] = 'BDX_BUS_SPLIT_COLLISION'
# create new id field
new_field_name = bus_lines_id_field_name + '_split'
bus_lines_split_at_collision_geolayer['metadata']['fields'][new_field_name] = \
    copy.deepcopy(bus_lines_split_at_collision_geolayer['metadata']['fields'][bus_lines_id_field_name])
if 'index' in bus_lines_split_at_collision_geolayer['metadata']['fields'][new_field_name]:
    bus_lines_split_at_collision_geolayer['metadata']['fields'][new_field_name]['index'] = \
        len(bus_lines_split_at_collision_geolayer['metadata']['fields'])-1

# loop on id_line_topo from bus line
new_i_feat = 0
for bus_line_i_feat, bus_line_feature in bus_lines_geolayer['features'].items():
    bus_line_feature_id_line_topo = bus_line_feature['attributes'][bus_lines_id_field_name]
    bus_line_feature_geometry = bus_line_feature['geometry']
    bus_line_feature_attributes = bus_line_feature['attributes']
    # get collision point and create a multipoint geometry from it :
    #    - get i_feat of bus_collision_geolayer that
    #    - recuperate geometry of each bus_collision_geolayer featue
    #    - create a new geometry  which is the result of collection points of the entities we just retrieved above
    multi_point_collision_geometry = {'type': 'MultiPoint', 'coordinates': []}
    for bus_collision_i_feat in bus_collision_id_line_index['index'][bus_line_feature_id_line_topo]:
        bus_collision_feature = bus_collision_geolayer['features'][bus_collision_i_feat]
        # get point geometry
        bus_collision_geometry = bus_collision_feature['geometry']
        # merge geometry to multi_point_collision_geometry
        multi_point_collision_geometry = geoformat.merge_geometries(
            multi_point_collision_geometry,
            bus_collision_geometry,
            bbox=False
        )
    # split bus line by multi_point_collision_geometry
    bus_line_feature_geometry_split_by_points = geoformat.linestring_split_by_point(
        bus_line_feature_geometry,
        multi_point_collision_geometry,
        tolerance=0.0000001,
        bbox=False)
    for i_split, bus_line_splited_geometry in enumerate(geoformat.multi_geometry_to_single_geometry(
            bus_line_feature_geometry_split_by_points)):
        bus_lines_split_at_collision_feature_attributes = copy.deepcopy(bus_line_feature_attributes)
        # write new id 'id_line_topo_split' in attributes
        bus_lines_split_at_collision_feature_attributes[new_field_name] = \
            bus_line_feature_id_line_topo + ':{i_split}'.format(i_split=i_split)
        # create new feature
        bus_lines_split_at_collision_feature = {"attributes": bus_lines_split_at_collision_feature_attributes,
                                                'geometry': bus_line_splited_geometry}
        # add to geolayer
        bus_lines_split_at_collision_geolayer['features'][new_i_feat] = bus_lines_split_at_collision_feature
        # increment new_i_feat
        new_i_feat += 1


# write new geolayer : bus_lines_split_at_collision_geolayer
geoformat.geolayer_to_geojson(
    geolayer=bus_lines_split_at_collision_geolayer,
    path=bus_lines_split_at_collision_path,
    overwrite=True
)


with open('data/FRANCE/BDX_BUS_COLLISION_split.yaml', 'w') as yaml_file:
    yaml_file.write(yaml.dump(bus_lines_split_at_collision_geolayer))


with open('data/FRANCE/BDX_BUS_COLLISION_split.yaml', 'r') as yaml_file:
    bus_lines_split_at_collision_geolayer_2 = yaml.load(yaml_file, Loader=yaml.FullLoader)


print(bus_lines_split_at_collision_geolayer == bus_lines_split_at_collision_geolayer_2)