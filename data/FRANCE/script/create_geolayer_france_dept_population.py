import geoformat
import geoformat.manipulation.geolayer_manipulation


def create_geolayer_fr_dept_population():

    path = 'data/FRANCE/COMMUNE_CARTO.shp'
    commune_data_geolayer = geoformat.ogr_layer_to_geolayer(path,
                                                  field_name_filter=['ID', 'INSEE_COM', 'INSEE_ARR', 'INSEE_DEP',
                                                                     'INSEE_REG', 'CODE_EPCI', 'NOM_COM',
                                                                     'NOM_COM_M', 'POPULATION'])

    dept_group_by = geoformat.clause_group_by(commune_data_geolayer, ['INSEE_DEP'])
    feature_list = [None] * len(dept_group_by)
    for i_dept, (code_dept, i_feat_list) in enumerate(dept_group_by.items()):
        code_dept = code_dept[0]
        dept_population = 0
        dept_area = 0

        for i_feat in i_feat_list:
            # get population
            feature_commune = commune_data_geolayer['features'][i_feat]
            commune_population = feature_commune['attributes']['POPULATION']
            dept_population += commune_population
            # compute area km²
            feature_geometry = feature_commune['geometry']
            feature_area = geoformat.geometry_area(feature_geometry)
            dept_area += feature_area

        # get REG number
        dept_reg_code = feature_commune['attributes']['INSEE_REG']
        # calculate area per km²
        dpt_area_km2 = round(dept_area / 1000000, 2)
        # calculate density per km²
        dept_density = round(dept_population / dpt_area_km2, 2)
        # create feature
        dept_feature = {
            "attributes": {"CODE_DEPT": code_dept, 'INSEE_REG': dept_reg_code, "POPULATION": dept_population,
                           "AREA": dpt_area_km2, "DENSITY": dept_density}
        }
        # store feature in list
        feature_list[i_dept] = dept_feature

    return geoformat.manipulation.geolayer_manipulation.feature_list_to_geolayer(feature_list, 'dept_population')


print(geoformat.__version__)
geolayer_fr_dept_population = create_geolayer_fr_dept_population()
print(geoformat.print_features_data_table(geolayer_fr_dept_population))
print(geolayer_fr_dept_population)
