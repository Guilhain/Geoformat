```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'fontFamily': 'CommitMono',
      'fontSize': '13',
      'primaryColor': '#ffffff',
      'primaryTextColor': '#000',
      'primaryBorderColor': '#525252',
      'clusterBorder': '#252525',
      'nodeBorder': '#737373'
    }
  }
}%%

flowchart
    classDef fixKeyStyle fill:#f7f7f7
    classDef keyStyle fill:transparent, stroke:transparent
    classDef typStyle fill:transparent, stroke:transparent
    classDef wipStyle fill:transparent, stroke-dasharray: 5
    
    M[metadata]
    MA(name)
    MB[fields]
    MC(geometry_ref)
    MCA(type)
    MCB(crs)
    MD(constraints):::wipStyle
    MDA(pk):::wipStyle
    MDB(fk):::wipStyle
    ME(index)
    MEA(attributes)
    MEAA["<i>field_name</i> [str]"]:::keyStyle
    MEAAA(metadata)
    MEAAAA(type)
    MEAAB(index)
    MEAABA["<i>field_value</i> [Union(all)]"]:::keyStyle
    MEB(geometry)
    MEBA(metadata)
    MEBAA(type)
    MEBAB(mesh_size)
    MEBAC(x_grid_origin)
    MEBAD(y_grid_origin)
    MEBAE(grid_precision)
    MEBB(index)
    MEBBA("<i>idx_id</i> [tuple(int, int)]"):::keyStyle
    MF(matrix)
    MFA(adjacency)
    MFAA(metadata)
    MFAAA(type)
    MFAB(matrix)
    MFABA("<i>i_feat</i> [int]"):::keyStyle
    MFB(distance):::wipStyle
    MFBA(metadata):::wipStyle
    MFBAA(type):::wipStyle
    MFBB(matrix):::wipStyle
    MFBBA("<i>i_feat</i> [int]"):::wipStyle
    MG(feature serialize)
    
    %%    type
    MAT("[str]"):::typStyle
    MCAT("[set(int)]"):::typStyle
    MCBT("[int]"):::typStyle
    MGT("[bool]"):::typStyle
    MEAAAAT("[str]"):::typStyle
    MEAABAT("List[Union[all]]"):::typStyle
    MEBAAT("[str]"):::typStyle
    MEBABT("[float]"):::typStyle
    MEBACT("[float]"):::typStyle
    MEBADT("[float]"):::typStyle
    MEBAET("[float]"):::typStyle
    MEBBAT("List[int]"):::typStyle
    MFAAAT("[str]"):::typStyle
    MFABAT("List[int]"):::typStyle
    MFBAAT("[str]"):::typStyle
    MFBBAT("List[int]"):::typStyle
    
    subgraph Metadata
        direction LR

    
        M --- MA
        M --- MB
        M --- MC
        MC --- MCA
        MC --- MCB 
        M --- MD
        MD --- MDA
        MD --- MDB
        M --- ME
        ME --- MEA
        MEA --- MEAA
        MEAA --- MEAAA
        MEAAA --- MEAAAA
        MEAAB --- MEAABA
        MEAA --- MEAAB
        ME --- MEB
        MEB --- MEBA
        MEBA --- MEBAA
        MEBA --- MEBAB
        MEBA --- MEBAC
        MEBA --- MEBAD
        MEBA --- MEBAE
        MEB --- MEBB
        MEBB --- MEBBA
        M --- MF
        MF --- MFA
        MFA --- MFAA
        MFAA --- MFAAA
        MFA --- MFAB
        MFAB --- MFABA
        MF --- MFB
        MFB --- MFBA
        MFBA --- MFBAA
        MFB --- MFBB
        MFBB --- MFBBA
        M --- MG
        
        MA -.- MAT
        MCA -.- MCAT
        MCB -.- MCBT
        MG -.- MGT
        MEAAAA -.- MEAAAAT
        MEAABA -.- MEAABAT
        MEBAA -.- MEBAAT
        MEBAB -.- MEBABT
        MEBAC -.- MEBACT
        MEBAD -.- MEBADT
        MEBAE -.- MEBAET
        MEBBA -.- MEBBAT
        MFAAA -.- MFAAAT
        MFABA -.- MFABAT
        MFBAA -.- MFBAAT
        MFBBA -.- MFBBAT
    
    end
```