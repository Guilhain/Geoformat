```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'fontFamily': 'CommitMono',
      'fontSize': '13',
      'primaryColor': '#ffffff',
      'primaryTextColor': '#000',
      'primaryBorderColor': '#525252',
      'clusterBorder': '#252525',
      'nodeBorder': '#737373'
    }
  }
}%%

flowchart
    classDef fixKeyStyle fill:#f7f7f7
    classDef keyStyle fill:transparent, stroke:transparent
    classDef typStyle fill:transparent, stroke:transparent
    classDef wipStyle fill:transparent, stroke-dasharray: 5

    D[Feature]
    DA(<i>attributes</i>):::fixKeyStyle
    DB(<i>geometry</i>):::fixKeyStyle
    DAA["<i>field_name</i> [str]"]:::keyStyle
    DBA(<i>type</i>):::fixKeyStyle
    DBB(<i>coordinates</i>):::fixKeyStyle
    
    DAAT("[Union[all]]"):::typStyle
    DBAT("[str]"):::typStyle
    DBBT("List[Union[float x dim]]"):::typStyle

    
    subgraph Feature
        D --- DA
        D --- DB
        DA --- DAA
        DAA -.- DAAT
        DB --- DBA
        DB --- DBB
        DBA -.- DBAT
        DBB -.- DBBT
    end

```