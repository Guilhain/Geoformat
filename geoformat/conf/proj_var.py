proj_correspondance = {
   "CRS84": 4326,         # (WGS 84 / Auto UTM )
   "CRS83": 2954,         # (NAD83 longitude-latitude)
   "CRS27": 4267,         # (NAD27 longitude-latitude)
   "NAD83": 2954,         # (NAD83 longitude-latitude)
   "NAD27": 4267,         # (NAD27 longitude-latitude)
   "AUTO2:42001": None,   # (WGS 84 / Auto UTM )
   "AUTO2:42002": 9807,   # (WGS 84 / Auto Tr. Mercator)
   "AUTO2:42003": 9840,   # (WGS 84 / Auto Orthographic)
   "AUTO2:42004": 4087,   # (WGS 84 / Auto Equirectangular)
   "AUTO2:42005": 54009,  # (WGS 84 / Auto Mollweide)
   "AUTO2:97001": None,   # Gnonomic
   "AUTO2:97002": 54026,  # Stereographic
   "AUTO2:97003": 54032,  # Azimuthal Equidistant
}