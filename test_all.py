try:
    from osgeo import ogr
    from osgeo import osr

    import_ogr_success = True
except ImportError:
    import_ogr_success = False

try:
    import psycopg2
    import psycopg2.extras

    import_psycopg2_success = True
except ImportError:
    import_psycopg2_success = False

try:
    import matplotlib
    import_matplotlib_success = True
except ImportError:
    import_matplotlib_success = False

try:
    import numpy

    import_numpy_success = True
except ImportError:
    import_numpy_success = False

from tests.geoformat.conf import test_format_data
from tests.geoformat.conf import test_path
from tests.geoformat.constraints import test_primary_key
from tests.geoformat.conversion import test_bbox_conversion
from tests.geoformat.conversion import test_bytes_conversion
from tests.geoformat.conversion import test_coordinates_conversion
from tests.geoformat.conversion import test_datetime_conversion
from tests.geoformat.conversion import test_feature_conversion
from tests.geoformat.conversion import test_fields_conversion
from tests.geoformat.conversion import test_geolayer_conversion
from tests.geoformat.conversion import test_geometry_conversion
from tests.geoformat.conversion import test_metadata_conversion
from tests.geoformat.conversion import test_precision_tolerance_conversion
from tests.geoformat.conversion import test_segment_conversion
from tests.geoformat.driver import test_csv_driver
from tests.geoformat.driver import test_esri_shapefile_driver
from tests.geoformat.driver import test_geojson_driver
from tests.geoformat.driver import test_postgresql_driver
from tests.geoformat.explore_data import test_duplicate
from tests.geoformat.explore_data import test_print_data
from tests.geoformat.geoprocessing.connectors import test_operations
from tests.geoformat.geoprocessing.connectors import test_predicates
from tests.geoformat.geoprocessing.generalization import test_ramer_douglas_peucker
from tests.geoformat.geoprocessing.generalization import test_visvalingam_whyatt
from tests.geoformat.geoprocessing.geoparameters import test_bbox
from tests.geoformat.geoprocessing.geoparameters import test_boundaries
from tests.geoformat.geoprocessing.geoparameters import test_lines
from tests.geoformat.geoprocessing.matrix import test_adjacency
from tests.geoformat.geoprocessing.measure import test_area
from tests.geoformat.geoprocessing.measure import test_distance
from tests.geoformat.geoprocessing.measure import test_length
from tests.geoformat.geoprocessing import test_area
from tests.geoformat.geoprocessing import test_length
from tests.geoformat.geoprocessing import test_line_merge
from tests.geoformat.geoprocessing import test_merge_geometries
from tests.geoformat.geoprocessing import test_point_on_linestring
from tests.geoformat.geoprocessing import test_simplify
from tests.geoformat.geoprocessing import test_split
from tests.geoformat.geoprocessing import test_union
from tests.geoformat.index.attributes import test_hash
from tests.geoformat.index.geometry import test_grid
from tests.geoformat.manipulation import test_feature_manipulation
from tests.geoformat.manipulation import test_geolayer_manipulation
from tests.geoformat.manipulation import test_metadata_manipulation
from tests.geoformat.obj import test_geometry
from tests.geoformat.processing.data.join import test_join
from tests.geoformat.processing.data.join import test_merge_objects
from tests.geoformat.processing.data import test_clauses
from tests.geoformat.processing.data import test_field_statistics
from tests.geoformat.processing.data import test_union


def test_all():
    test_format_data.test_all()
    test_path.test_all()
    test_primary_key.test_all()
    test_bbox_conversion.test_all()
    test_bytes_conversion.test_all()
    test_coordinates_conversion.test_all()
    test_datetime_conversion.test_all()
    test_feature_conversion.test_all()
    test_fields_conversion.test_all()
    test_geolayer_conversion.test_all()
    test_geometry_conversion.test_all()
    test_metadata_conversion.test_all()
    test_precision_tolerance_conversion.test_all()
    test_segment_conversion.test_all()
    test_csv_driver.test_all()
    test_esri_shapefile_driver.test_all()
    test_geojson_driver.test_all()
    test_postgresql_driver.test_all()
    test_duplicate.test_all()
    test_print_data.test_all()
    test_operations.test_all()
    test_predicates.test_all()
    test_ramer_douglas_peucker.test_all()
    test_visvalingam_whyatt.test_all()
    test_bbox.test_all()
    test_boundaries.test_all()
    test_lines.test_all()
    test_adjacency.test_all()
    test_area.test_all()
    test_distance.test_all()
    test_length.test_all()
    test_area.test_all()
    test_length.test_all()
    test_line_merge.test_all()
    test_merge_geometries.test_all()
    test_point_on_linestring.test_all()
    test_simplify.test_all()
    test_split.test_all()
    test_union.test_all()
    test_hash.test_all()
    test_grid.test_all()
    test_feature_manipulation.test_all()
    test_geolayer_manipulation.test_all()
    test_metadata_manipulation.test_all()
    test_geometry.test_all()
    test_join.test_all()
    test_merge_objects.test_all()
    test_clauses.test_all()
    test_field_statistics.test_all()
    test_union.test_all()


if __name__ == "__main__":
    test_all()
